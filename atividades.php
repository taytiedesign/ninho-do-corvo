<?php include 'header.php';?>
	<section id="content" class="atividades">
		<!-- MATAGAL -->
		
		<article class="traking">
			<figure>
				<img class="lazy" src="img/atividades/trekking.png" alt="">
			</figure>
		</article>
		<article class="equipamento">
			<figure>
				<img class="lazy" src="img/atividades/equip.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/atividades/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-atividades">
				<h1>Atividades</h1>
				<div class="frase">
					Além de oferecermos nossas trilhas para contemplação da vida silvestre, temos atividades de<br>aventura que vão deixar a sua visita ao Ninho do Corvo ainda mais divertida e emocionante. Com<br>uma equipe especialmente treinada para exercer as suas funções, e equipamentos certificados pelos<br>órgãos competentes, garantimos o máximo de segurança nas atividades oferecidas.
				</div>
			</div>

			<div class="galeria-atividades">
				<div class="menu">

					<div class="tabs">
					  <ul>
					    <li><a href="#atividades-1">Todas as Atividades</a></li>
					    <li><a href="#atividades-2">Atividades Regulares</a></li>
					    <li><a href="#atividades-3">Atividades em Grupo</a></li>
					    <li><a href="#atividades-4">Atividades com Hospedagem</a></li>
					    <li><a href="#atividades-5">Combo de Atividades</a></li>
					  </ul>
					  <div id="atividades-1">

					  	<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>
						<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>
					  	<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>
					  	<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>
					  	<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>

					  </div>
					  <div id="atividades-2">
					  	
						<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>
					  	<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>

					  </div>
					  <div id="atividades-3">
					  	
						<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>

					  </div>
					  <div id="atividades-4">
					  	
						<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>
					  	<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>
					  	<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>

					  </div>
					  <div id="atividades-5">
					  	
					  	<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>
					  	<div class="entry">
					  		<div class="hover">
					  			<div class="legenda">
					  				Caminhos Andinos Expedição Altiplano
					  			</div>
					  			<a href="atividades-interna.php">
					  				Veja Mais
					  			</a>
					  		</div>
					  		<img src="img/atividades/1.jpg" alt="">
					  	</div>

					  </div>
					 
					</div>
					<div class="paginacao">
						<a href="#" class="select">1</a>
						<a href="#">2</a>
					</div>

				</div>	
			</div>

			
		

			
<?php include 'footer.php';?>