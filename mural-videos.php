<?php include 'header.php';?>
	<section id="content" class="mural-videos">
		<!-- MATAGAL -->
		<article class="fotos-mural">
			<figure>
				<img class="lazy" src="img/mural/fotos.png" alt="">
			</figure>
		</article>
		<article class="mochileiro">
			<figure>
				<img class="lazy" src="img/mural/mochileiro.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/mural/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-mural">
				<h1>Videos</h1>
				<a href="envia-mural.php" class="envie">
					Enviar
				</a>
			</div>

			<div class="zoom-videos">
				Espaço para receber o Video
			</div>

			<div class="mural-depoimento-videos">

				<div class="mais-videos">
					Mais videos do Ninho!
				</div>

				<div class="registros-videos">
					
					<ul id="mycarousel2" class="jcarousel-skin-tango">

							<li>
								<div class="item">
									<div class="hover">
										<a href="#">
											<img src="img/mural/hover-video.png" alt="">
										</a>
									</div>
									<img src="img/mural/video.jpg" alt="">
								</div>
							</li>
					</ul>

				</div>
				<div class="page">
					<ul>
						<li>
							<a href="#" class="selected">1</a>
						</li>
						<li>
							<a href="#">2</a>
						</li>
						<li>
							<a href="#">3</a>
						</li>
						<li>
							<a href="#">4</a>
						</li>
						</ul>
					</div>
			</div>


			
		

			
<?php include 'footer.php';?>