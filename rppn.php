<?php include 'header.php';?>
	<section id="content" class="rppn">
		<!-- MATAGAL -->
		<article class="capaceteRight">
			<figure>
				<img src="img/home/capacete.png" alt="">
			</figure>
		</article>
		
		<article class="alpinista">
			<figure>
				<img class="lazy" src="img/rppn/alpinista.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->
		<article class="banner-interno">
			<img src="img/rppn/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			<div class="rppn">
				<h1>A RPPN Ninho do Corvo</h1>
				<div class="texto">
					<p>Em dezembro de 2007,  cerca de 55% da área total da propriedade foi declarada RPPN – Reserva Particular do Patrimônio Natural, através de decreto  do Governo do Estado do Paraná, o que representa que estes 10,55 hectares não podem de forma alguma sofrer qualquer tipo de agressão, as únicas finalidades a que se destinam estas áreas são as de conservacionismo, estudo e turismo de forma controlada e organizada para que não cause interferências no ambiente.</p>
				</div>
				<div class="texto">
					<p>Em dezembro de 2007,  cerca de 55% da área total da propriedade foi declarada RPPN – Reserva Particular do Patrimônio Natural, através de decreto  do Governo do Estado do Paraná, o que representa que estes 10,55 hectares não podem de forma alguma sofrer qualquer tipo de agressão, as únicas finalidades a que se destinam estas áreas são as de conservacionismo, estudo e turismo de forma controlada e organizada para que não cause interferências no ambiente.</p>
				</div>
			</div>
			<div class="banner-middle">
				<img src="img/rppn/middle.png" alt="">
			</div>
			
			<div class="fotos1">
				<p>A importância da reserva se deve além da beleza cênica do local, com varias cachoeiras, ela guardar espécies da flora e fauna em perigo de extinção como  xaxins, imbuias, araucárias, bem como é uma área de grande atrativo geológico devido as suas formações de basalto ao longo do curso do rio Barra Bonita. É importante também ressaltar que os proprietários entendem que a grande vocação da região é para o ecoturismo e  turismo sem conservacionismo é algo praticamente impossível e a RPPN tem também como missão passar os conceitos de conservação e utilização sustentável para a comunidade, de forma que esta seja,  através do exemplo,  objeto de transformação da mesma no que se refere a estes temas.</p>
			</div>

			<div class="banner-bottom">
				<img src="img/rppn/bottom.png" alt="">
			</div>

			<div class="fotos2">
				<p>Também é nossa missão tentar passar aos nossos visitantes, a importância de se procurar manter a natureza como está e se possível recuperá-la. A RPPN está cercada de ameaças como  agricultura com uso intensivo de agrotóxicos, queimadas e  caça ilegal, que podem ser temas para  reflexão durante a visita.
Dentro da própria RPPN muitas áreas sofreram queimadas e lavouras, mas é muito interessante ir verificando o processo de auto-recuperação destas áreas ao longo  do tempo e cremos que esta constatação pode levar o visitante a entender que além de manter também podemos recuperar</p>
				<h3>A RPPN Ninho do Corvo está aberta aos pesquisadores de todo o Brasil e<br>exterior que desejem realizar trabalhos de pesquisa na área.<br>
Na medida de nossa capacidade procuramos contribuir para estes<br>nos quesitos de hospedagem, alimentação sempre que possível.</h3>
			</div>

			
		

			
<?php include 'footer.php';?>