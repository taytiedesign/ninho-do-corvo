<?php include 'header.php';?>
	<section id="content" class="turismo">
		<!-- MATAGAL -->
		<article class="carro-turismo">
			<figure>
				<img class="lazy" src="img/turismo/carro.png" alt="">
			</figure>
		</article>
		<article class="bike-turimo">
			<figure>
				<img class="lazy" src="img/turismo/bike.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/turismo/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-turismo">
				<h1>Turismo Ativo</h1>
				<div class="frase">
					As atividades de Turismo Ativo priorizam uma experiência diferenciada. Nessas atividades<br>nossos clientes interagem com os lugares, pessoas e ambientes visitados. Com grupos<br>pequenos, guias especilaizados, roteiros exclusivos e customizados norteados por nosso<br>parceiro Terra e Asfalto Expedições na América do Sul e no município de Prudentópolis.
				</div>
			</div>

			<div class="news-turismo">
				<div class="banner">
					<div class="legenda">
						<div class="info">
							<div>
								<h1>Caminhos Andinos Expedição Altiplano</h1>
								<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tempor nec dolor in elementum. Maecenas egestas tempor rhoncus. Sed nec ante augue. Pellentesque pretium nibh sed vehicula rhoncus. Quisque pulvinar tristique mattis. In eget ipsum vehicula, tempor diam vitae, fringilla ipsum posuere.</span>
							</div>
						</div>
						<div class="icon">
							<div>
								<a href="turismo-ativo-interna.php">
									<img src="img/turismo/icon-ir.png" alt="">
								</a>
							</div>
						</div>
					</div>
					<img src="img/turismo/Img-turismo.jpg" alt="">
				</div>
				<div class="thumbs">
					<div class="legenda">
						<div class="info">
							<div>
								Caminhos Andinos Expedição Altiplano
							</div>
						</div>
						<div class="icon">
							<div>
								<a href="turismo-ativo-interna.php">
									<img src="img/turismo/icon-ir-menor.png" alt="">
								</a>
							</div>
						</div>
					</div>
					<img src="img/turismo/Peq-turismo.jpg" alt="">
				</div>
				<div class="thumbs">
					<div class="legenda">
						<div class="info">
							<div>
								Caminhos Andinos Expedição Altiplano
							</div>
						</div>
						<div class="icon">
							<div>
								<a href="turismo-ativo-interna.php">
									<img src="img/turismo/icon-ir-menor.png" alt="">
								</a>
							</div>
						</div>
					</div>
					<img src="img/turismo/Peq-turismo.jpg" alt="">
				</div>
				<div class="thumbs">
					<div class="legenda">
						<div class="info">
							<div>
								Caminhos Andinos Expedição Altiplano
							</div>
						</div>
						<div class="icon">
							<div>
								<a href="turismo-ativo-interna.php">
									<img src="img/turismo/icon-ir-menor.png" alt="">
								</a>
							</div>
						</div>
					</div>
					<img src="img/turismo/Peq-turismo.jpg" alt="">
				</div>
			</div>


			<div class="sobre-turismo-ativo">
				<div class="texto">
					<figure>
						<img src="img/turismo/imagem1.jpg" alt="">
					</figure>
					<p>Márcio Canto de Miranda, fundador do Ninho do Corvo, desde 2000 realiza expedições pela Cordilheira dos Andes, tendo realizado diversas viagens para a região, passando pela Argentina, Chile e Perú e visitando lugares famosos como a Patagônia, Los Glaciares, Deserto do Atacama, Macchu Picchu, Terra do Fogo, Lago Titicaca dentre outros. </p>
					<p>Mas ele considera que os lugares desconhecidos são tão ou até mais interessantes que estes famosos.</p>
					<p>Lugares como Cerro Tuzgle, Iruya, Cachi, Cuesta Del Obispo, Quebrada de Humauaca, Salta, Villa La Angostura, Pucón, Salinas Grandes, Quebrada Del Toro, somente para citar alguns, são lugares que na visão de Márcio valem a pena conhecer.</p>
					<p>A partir de 2004, Márcio começou a levar pessoas comercialmente em suas viagens, e desde então vem procurando desenvolver roteiros fora do lugar comum, evitando lugares massificados e sempre procurando colocar nestes roteiros experiências diferentes e serviços de alto valor agregado para seus clientes, transformando cada viagem numa experiência única de viagem.</p>
				</div>
				<div class="texto">
					<figure>
						<img src="img/turismo/imagem2.jpg" alt="">
					</figure>
					<p>Alguns conceitos que são padrão nos roteiros desenvolvidos são: grupos pequenos de no máximo 14 pessoas, atividades sempre exclusivas para o grupo com a presença de guia brasileiro, guia local exclusivo do grupo, transportes exclusivos, e principalmente a liberdade de se fazer o que se quer nos roteiros, sem pressões.</p>
					<p>O Terra & Asfalto Expedições é desta forma o braço explorador e viajante da RPPN Ninho do Corvo. Para nós é uma extensão daquilo que fazemos na reserva, e fazemos com muito prazer porque gostamos do que fazemos. E com este amor é que criamos o roteiro “Caminhos Andinos” um giro pelo Andes Argentinos, que pode ser realizado de carro em cerca de 15 dias, chegando até o Deserto do Atacama no Chile.</p>
					<p>Para participar deste roteiro existem datas de saída onde tentamos fechar grupos, ou montar saídas exclusivas para grupos particulares podendo ser rodoviários em carros particulares, ou aéreo.
</p>
					<p>Além do Caminhos Andinos, temos opções de roteiros rodoviários para Macchu Picchu, Patagônia Andina e Fueguina, Lagos Andinos,Costa Chilena, Uruguay , e desenvolvemos roteiros específicos, conforme o interesse do grupo.</p>
				</div>
			</div>

			
		

			
<?php include 'footer.php';?>