<?php include 'header.php';?>
	<section id="content" class="perguntas">
		<!-- MATAGAL -->
		<article class="equipamentoRight">
			<figure>
				<img src="img/perguntas/equip.png" alt="">
			</figure>
		</article>
		
		<article class="carro">
			<figure>
				<img class="lazy" src="img/perguntas/carro.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->
		<article class="banner-interno">
			<img src="img/perguntas/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-perguntas">
				<h1>Perguntas Frequentes</h1>
				<div class="texto">
					<ul>
						<li><a href="#resposta-1">Como é o funcionamento do Ninho do Corvo?</a></li>
						<li><a href="#resposta-2">Quais os dias de funcionamento do Ninho do Corvo?</a></li>
						<li><a href="#resposta-3">É necessário pagar algo para ter entrada no Ninho do Corvo?</a></li>
						<li><a href="#resposta-4">Como é o pacote de visitação e atividades oferecido no Ninho do Corvo?</a></li>
						<li><a href="#resposta-5">Quais os valores para a prática das atividades?</a></li>
						<li><a href="#resposta-6">Nunca pratiquei atividades de aventura, tenho condições de praticar as oferecidas no Ninho do Corvo?</a></li>
						<li><a href="#resposta-7">Quais são as condições para a prática das atividades oferecidas no Ninho do Corvo?</a></li>
						<li><a href="#resposta-8">Já pratiquei muitas atividades de aventura em muitos lugares, por que devo praticar as atividades oferecidas no Ninho do Corvo?</a></li>
					</ul>
				</div>
				<div class="texto">
					<ul>
						<li><a href="#resposta-9">Qual a faixa etária que pode praticar atividades no Ninho do Corvo?</a></li>
						<li><a href="#resposta-10">Já pratiquei muitas atividades de aventura em muitos lugares, por que devo praticar as atividades oferecidas no Ninho do Corvo?</a></li>
						<li><a href="#resposta-11">É seguro fazer as atividades oferecidas no Ninho do Corvo?</a></li>
						<li><a href="#resposta-12">O que eu preciso levar para uma visita ao Ninho do Corvo?</a></li>
						<li><a href="#resposta-13">Tenho onde me hospedar no Ninho do Corvo?</a></li>
						<li><a href="#resposta-14">É possível acampar no Ninho do Corvo?</a></li>
						<li><a href="#resposta-15">Posso fazer as minhas refeições no Ninho do Corvo?</a></li>
						<li><a href="#resposta-16">Vocês trabalham com grupos?</a></li>
					</ul>
				</div>
			</div>
			
			<div class="respostas-perguntas">
				<div class="accordion">
					<!-- 1 -->
					<h5 id="resposta-1">Como é o funcionamento do Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 1 -->

					<!-- 2 -->
					<h5 id="resposta-2">Quais os dias de funcionamento do Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 2 -->

					<!-- 3 -->
					<h5 id="resposta-3">É necessário pagar algo para ter entrada no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 3 -->

					<!-- 4 -->
					<h5 id="resposta-4">Como é o pacote de visitação e atividades oferecido no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 4 -->

					<!-- 5 -->
					<h5 id="resposta-5">Quais os valores para a prática das atividades?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 5 -->

					<!-- 6 -->
					<h5 id="resposta-6">Nunca pratiquei atividades de aventura, tenho condições de praticar as oferecidas no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 6 -->

					<!-- 7 -->
					<h5 id="resposta-7">Quais são as condições para a prática das atividades oferecidas no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 7 -->

					<!-- 8 -->
					<h5 id="resposta-8">Já pratiquei muitas atividades de aventura em muitos lugares, por que devo praticar as atividades oferecidas no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 8 -->

					<!-- 9 -->
					<h5 id="resposta-9">Qual a faixa etária que pode praticar atividades no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 9 -->

					<!-- 10 -->
					<h5 id="resposta-10">Já pratiquei muitas atividades de aventura em muitos lugares, por que devo praticar as atividades oferecidas no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 10 -->

					<!-- 11 -->
					<h5 id="resposta-11">É seguro fazer as atividades oferecidas no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 11 -->

					<!-- 12 -->
					<h5 id="resposta-12">O que eu preciso levar para uma visita ao Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 12 -->

					<!-- 13 -->
					<h5 id="resposta-13">Tenho onde me hospedar no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 13 -->

					<!-- 14 -->
					<h5 id="resposta-14">É possível acampar no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 14 -->

					<!-- 15 -->
					<h5 id="resposta-15">Posso fazer as minhas refeições no Ninho do Corvo?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 15 -->

					<!-- 16 -->
					<h5 id="resposta-16">Vocês trabalham com grupos?</h5>
					<div>
					    <p>
					    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer
					    ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit
					    amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut
					    odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
					    </p>
					</div>
					<!-- 16 -->
				</div>
			</div>			

<?php include 'footer.php';?>