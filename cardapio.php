<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<style>
*
{
	margin: 0;
	padding: 0;
}

body
{
	background: #e0d8bf;
}

.intro-contato
{
	width: 920px;
}

.intro-contato h1
{
	width: 920px;
	height: auto;
	text-align: center;
	color: #377852;
	font-size: 50px;
	margin: 20px 0 30px 0;
	font-family: 'Oregano', cursive;
	text-shadow: 1px 1px 0px rgba(255,255,255,1);
}

.formulario-campos
{
	float: left;
	width: 900px;
	height: auto;
	font-family: Tahoma;
	font-size: 16px;
	color: #4c4c4c;
	text-align: left;
	margin-top: 30px;
	margin-left: 20px;
}
</style>
<body>
	
	<div style="float: left; width: 920px; height: 300px;">
		<div class="intro-contato">
			<h1>Cardápio</h1>
		</div>
		<div class="formulario-campos">
			ARROZ E FEIJÃO<br>
			PRATO PRINCIPAL: CARNE PICADA COM ABÓBORA/ROLÊ DE CARNE<br>
			PTS COM VAGEM<br>
			SALADA: CENOURA RALADA<br>
			SOBREMESA: MAÇÃ<br>
			SUCO: UVA
		</div>
	</div>

</body>
</html>