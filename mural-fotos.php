<?php include 'header.php';?>
	<section id="content" class="mural">
		<!-- MATAGAL -->
		<article class="fotos-mural">
			<figure>
				<img class="lazy" src="img/mural/fotos.png" alt="">
			</figure>
		</article>
		<article class="mochileiro">
			<figure>
				<img class="lazy" src="img/mural/mochileiro.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/mural/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-mural intro-mural-interna">
				<h1>Imagens</h1>
				<a href="envia-mural.php" class="envie">
					Enviar
				</a>
			</div>

			<div class="todas-fotos-mural">
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="#">
							<img src="img/mural/hover.png" alt="">
						</a>
					</div>
					<img src="img/mural/foto.jpg" alt="">
				</div>
				<div class="page">
					<ul>
						<li>
							<a href="#" class="selected">1</a>
						</li>
						<li>
							<a href="#">2</a>
						</li>
						<li>
							<a href="#">3</a>
						</li>
						<li>
							<a href="#">4</a>
						</li>
					</ul>
				</div>
			</div>			
		

			
<?php include 'footer.php';?>