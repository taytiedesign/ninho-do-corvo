<?php include 'header.php';?>
	<section id="content" class="meus-pedidos">
		<!-- MATAGAL -->
		<article class="mochileiro">
			<figure>
				<img class="lazy" src="img/mural/mochileiro.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/minha-conta/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-pedidos">
				<h1>Carrinho de Compras</h1>
			</div>

			<div class="lista-pedidos">
				<div class="titulo">
					<div class="categoria">
						Produtos
					</div>
					<div class="categoria">
						Valor
					</div>
				</div>
				<div class="lista-produtos">
					<div class="o-produto">
						<a href="#">
							<img src="img/minha-conta/foto.jpg" alt="">
						</a>
						<ul>
							<li>Nome da Atividade / Combo</li>
							<li>Hospedagem - de 12/05/2013  a 18/05/2013</li>
							<li>6 Pessoas</li>
							<li>2 acompanhantes</li>
						</ul>
					</div>
					<div class="desconto">
						-R$ 50,00
					</div>
					<div class="desconto">
						R$ 600,00
					</div>
				</div>

				<div class="lista-produtos">
					<div class="o-produto">
						<a href="#">
							<img src="img/minha-conta/foto.jpg" alt="">
						</a>
						<ul>
							<li>Nome da Atividade / Combo</li>
							<li>Hospedagem - de 12/05/2013  a 18/05/2013</li>
							<li>6 Pessoas</li>
							<li>2 acompanhantes</li>
						</ul>
					</div>
					<div class="desconto">
						-R$ 50,00
					</div>
					<div class="desconto">
						R$ 600,00
					</div>
				</div>

				<div class="lista-produtos">
					<div class="o-produto">
						<a href="#">
							<img src="img/minha-conta/foto.jpg" alt="">
						</a>
						<ul>
							<li>Nome da Atividade / Combo</li>
							<li>Hospedagem - de 12/05/2013  a 18/05/2013</li>
							<li>6 Pessoas</li>
							<li>2 acompanhantes</li>
						</ul>
					</div>
					<div class="desconto">
						-R$ 50,00
					</div>
					<div class="desconto">
						R$ 600,00
					</div>
				</div>

				<div class="subtotal">
					<ul>
						<li>Subtotal:</li>
						<li>R$ 1200,00</li>
					</ul>
				</div>

				<div class="cupom">
					<form action="#">
						<label for="">
							Cupom de Desconto:
							<input type="text" name="cupom">
						</label>
						<input type="submit" name="gravar-cupom">
					</form>
					<div class="total">
						<ul>
							<li>
								-R$ 100,00
							</li>
							<li>
								<span>Total:</span>
								R$ 1200,00
							</li>
						</ul>
					</div>
				</div>

				<div class="regras">
					<a href="#">
						Dúvidas sobre Como Comprar?
					</a>
					<ul>
						<li>
							<a href="#">
								<img src="img/minha-conta/continue.png" alt="">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="img/minha-conta/comprar.png" alt="">
							</a>
						</li>
					</ul>
					<div class="alerta">
						<div>
							<img src="img/minha-conta/cadeado.png" alt="">
							Pagamento em ambiente seguro via PagSeguro
						</div>
					</div>
				</div>

				
			</div>

					
		

			
<?php include 'footer.php';?>