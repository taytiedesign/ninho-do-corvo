<?php include 'header.php';?>
	<section id="content" class="cadastro">
		<!-- MATAGAL -->
		<article class="ave-contato">
			<figure>
				<img class="lazy" src="img/cadastro/ave.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/cadastro/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-minha-conta">
				<h1>Área do Usuário</h1>
				<div class="saudacao">
					<ul>
						<li>Bem Vindo (a)</li>
						<li>Leonardo Taytie</li>
					</ul>
				</div>
			</div>

			<div class="minha-conta">
				<div class="change">
					<ul>
						<li>
							<a href="minha-conta.php">
								Alterar Dados de Cadastro
							</a>
						</li>
						<li>
							<a href="meus-pedidos.php" class="selected">
								Meus Pedidos
							</a>
						</li>
						<li>
							<a href="index.php">
								Sair
							</a>
						</li>
					</ul>
				</div>
				<div class="formulario-minha-conta">
					<div class="detalhes-pedido">
						<div class="titulo">
							Detalhes do Pedido
							<a href="javascript:history.go(-1);">
								<img src="img/minha-conta/voltar.png" alt="">
							</a>
						</div>
						<strong>Pedido Nº:</strong> 2013000003<br>
						<strong>Adicionado em:</strong> 10/10/2013<br>
						<strong>Status do Pedido:</strong> Aguardando Pagamento<br>
						<strong>Status do PagSeguro:</strong> Não Pago<br>
						<a href="#">
							<img src="img/minha-conta/pagar-2.png" alt="">
						</a>
					</div>
					<ul>
						<li>Nº Pedido</li>
						<li>Data</li>
						<li>Status</li>
						<li>Valor</li>
					</ul>
					<div class="lista">
						<div>
							2013000003
						</div>
						<div>
							10/10/2013
						</div>
						<div>
							Aguardando Pagamento
						</div>
						<div>
							R$ 75,00
						</div>
						<div>
							<a href="#">
								<img src="img/minha-conta/pagar.png" alt="">
							</a>
							<a href="ver-detalhes.php">
								<img src="img/minha-conta/ver-detalhes.png" alt="">
							</a>
						</div>
					</div>
					<div class="final">
						<a href="#"><img src="img/minha-conta/comprar-produtos.png" alt=""></a>
						<a href="#"><img src="img/minha-conta/atualizar-lista.png" alt=""></a>
					</div>
				</div>
				
			</div>

			
			
<?php include 'footer.php';?>