<?php include 'header.php';?>
	<section id="content" class="atividades-interna">
		<!-- MATAGAL -->
		<article class="traking">
			<figure>
				<img class="lazy" src="img/atividades/trekking.png" alt="">
			</figure>
		</article>
		<article class="equipamento">
			<figure>
				<img class="lazy" src="img/atividades/equip.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="middle clearfix">
			
			<div class="intro-atividades-interno">
				<h1>Caminhos Andinos Expedição Altiplano</h1>
			</div>

			<div class="galeria-atividades-interna">
					
				<div id="gallery" class="ad-gallery">
			      
			      <div class="ad-image-wrapper"></div>

			      <div class="ad-nav">
			        <div class="ad-thumbs">
			          <ul class="ad-thumb-list">
			            <li>
			              <a href="images/1.jpg">
			                <img src="images/thumbs/t1.jpg">
			              </a>
			            </li>
			            <li>
			              <a href="images/2.jpg">
			                <img src="images/thumbs/t2.jpg">
			              </a>
			            </li>
			            <li>
			              <a href="images/3.jpg">
			                <img src="images/thumbs/t3.jpg">
			              </a>
			            </li>
			            <li>
			              <a href="images/4.jpg">
			                <img src="images/thumbs/t4.jpg">
			              </a>
			            </li>
			            <li>
			              <a href="images/5.jpg">
			                <img src="images/thumbs/t5.jpg">
			              </a>
			            </li>  
			            <li>
			              <a href="images/6.jpg">
			                <img src="images/thumbs/t6.jpg">
			              </a>
			            </li>
			          </ul>
			        </div>
			      </div>
			    </div>

			</div>

			<div class="info-atividades-interna">
				<div class="descricao">
					<h2>Descrição</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis augue tellus. Nam euismod mi ut felis mollis fermentum. Aenean faucibus sem ipsum, quis adipiscing est commodo quis. Aenean tincidunt dolor id cursus ultricies. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean venenatis quis nibh eu condimentum. Nulla at venenatis lectus, a malesuada ante. Nam vel lorem sagittis, tincidunt dolor a, tempor lacus. Suspendisse potenti. Aenean eget ante id lectus rutrum pharetra. Curabitur bibendum, justo quis fermentum rhoncus, quam lacus imperdiet diam, vitae volutpat orci lacus ac nisi. Sed lectus nulla, egestas ac neque in, luctus pharetra massa. Nunc ac tortor in orci lobortis accumsan sed vitae neque.</p>
					<p>Fusce a leo ut ligula euismod congue in ut metus. Duis scelerisque ligula at nibh porttitor, sit amet elementum magna laoreet. Aenean eu interdum orci, quis aliquet mi. Fusce leo risus, rutrum id arcu ac, consequat pharetra purus. In egestas ante a sem gravida commodo. Aliquam varius sem a dui posuere mollis. Praesent dolor quam, pretium eget risus vitae, ultrices pellentesque leo. Donec nec diam ut arcu cursus blandit quis non urna. Etiam vel vulputate metus.</p>
					<h2>Mapa</h2>
					
					
					 
					<div id="mapview">
					    <div id="map_canvas" style="float: left; width: 460px; height: 340px;"></div>
					    <div class="direcao" style="float: left; width: 460px; height: 340px; overflow: scroll; display:none; background: #fff; margin-top: 20px;">
					        <div id="directionsPanel" style="width: 460px; height; 100px; float: left; margin-top:-10px;"></div>
					    </div>
					</div>
					

				</div>
				<div class="descricao">
					<div class="valores">
						<h1>R$ 500,00</h1>
						<div class="info">
							Valor unitário referente para até<br>
							<span>5 pessoas</span>
						</div>
					</div>
					<form action="#">
						<fieldset>
							<table width="460" border="0" cellspacing="0" cellpadding="0">
							  <tr>
							    <td colspan="2">
							    	Qtd.: <input type="text" name="quantidade">
							    </td>
							  </tr>
							  <tr>
							    <td colspan="2">
							    	<span>Adicionar mais participantes (R$75,00 por pessoa):</span>
							    </td>
							  </tr>
							  <tr>
							    <td>
							    	Qtd.: <input type="text" name="quantidadeParticipantes">
							    </td>
							    <td>
							    	Máx.: 50 pessoas por atividade
							    </td>
							  </tr>
							  <tr>
							    <td colspan="2">
							    	<span>Adicionar não-participantes (R$75,00 por pessoa):</span>
							    </td>
							  </tr>
							  <tr>
							    <td>
							    	Qtd.: <input type="text" name="quantidadeAcompanhantes"><br>
							    </td>
							    <td>
							    	Máx.: 2 acompanhantes por atividade
							    </td>
							  </tr>
							  <tr>
							    <td colspan="2"><a href="#" class="regras-acompanhantes">Veja as regras de conduta para acompanhantes</a></td>
  							  </tr>
							  <tr>
							    <td colspan="2">
							    	Serviços Adicionais
							    	<a href="#" class="regras">Veja as regras de Hospedagem</a>
							    </td>
							  </tr>
							  <tr>
							    <td colspan="2">
							    	<div>
							    		<input type="checkbox" class="styled" name="tipo" value="hospedagem" id="hospedagem">
							    		Hospedagem
							    	</div>
							    	<div>
										<input type="checkbox" class="styled" value="camping" id="camping">
										Camping
									</div>
							    </td>
							  </tr>

							</table>

							<div class="servicos">
								Período de estadia:<br>
							    <label for="from">Entrada:</label><br>
							    <input type="text" name="entrada" id="from"><br><br>
							    <label for="to">Saída:</label><br>
							    <input type="text" name="saida" id="to">

							    <table width="320" border="0" cellspacing="0" cellpadding="0">
								  <tr>
								    <td>Hospedes:</td>
								    <td><input type="text" name="qtdHosp"></td>
								    <td>Máx. 4 pessoas</td>
								  </tr>
								</table>
								<input type="submit" name="check" value="OK">

							</div>

							<div class="alert">
								Camping / Hospedagem não disponível para compra online nesta data.<br>Selecione outra data ou siga a compra sem o Camping / Hospedagem.
							</div>
							
							<div class="alimentacao">
								<table width="460" border="0" cellspacing="0" cellpadding="0">
								  <tr>
								    <td>
								    	<span>Alimentação (R$ 10,00 por pessoa):</span>
								    	<a href="#" class="regras-cardapio">Veja o Cardápio</a></td>
								  </tr>
								  <tr>
								    <td>
								    	<label for="sim">
											<input type="radio" name="cardapio" value="Sim" id="sim" class="styled">
											Sim
										</label>
										<label for="nao">
											<input type="radio" name="cardapio" value="Nao" id="nao" class="styled">
											Não
										</label>
								    </td>
								  </tr>
								  <tr>
								    <td>
								    	<span>Data das Atividades:</span>
								    </td>
								  </tr>
								  <tr>
								    <td>
								    	<label for="agendarAgora">
								    		<input type="radio" name="datas" value="Agendar Agora" id="agendarAgora" class="styled">
								    		Agendar Agora
								    	</label>
										<label for="agendarDepois">
											<input type="radio" name="datas" value="Agendar Depois" class="styled" id="agendarDepois">Agendar Depois
										</label>
								    </td>
								  </tr>
								</table>
								<div class="alert">
									Voce selecionou uma hospedagem. Por favor, agende suas atividades agora.
								</div>
								<input type="submit" name="comprar" value="Comprar">
							</div>

						</fieldset>
					</form>
				</div>
			</div>

			

			
		

			
<?php include 'footer.php';?>