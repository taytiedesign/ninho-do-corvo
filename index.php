<?php include 'header.php';?>
	<section id="content" class="home">
		<!-- MATAGAL -->
		<article class="capacete">
			<figure>
				<img src="img/home/capacete.png" alt="">
			</figure>
		</article>
		<article class="bike">
			<figure>
				<img class="lazy" src="img/home/bike.png" alt="">
			</figure>
		</article>
		<article class="rapel">
			<figure>
				<img class="lazy" src="img/home/rapel.png" alt="">
			</figure>
		</article>
		<article class="tirolesa">
			<figure>
				<img class="lazy" src="img/home/tirolesa.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->
		<article class="banner">
			<div class="placa">
				<img src="img/home/placa.png" alt="">
			</div>
			<figure>
				<img src="img/home/banner.png" alt="">
			</figure>
		</article>
		<article class="middle clearfix">
			<article class="galeria">
				<h1>Atividades em Destaque</h1>
				<div class="content">

					<div class="entry">
						<div class="hover">
							<div>
								<span>Rapel no Salto Sete</span>
								<p>Rapel em uma excitante queda de 50 metro. Pacote mínimo com 5 participante(s)</p>
								<h3>R$ 500,00</h3>
							</div>
						</div>
						<figure>
							<a href="#">
								<img src="img/home/galeria/thumb/thumb.jpg" alt="">
							</a>
						</figure>
					</div>

					<div class="entry">
						<div class="hover">
							<div>
								<span>Rapel no Salto Sete</span>
								<p>Rapel em uma excitante queda de 50 metro. Pacote mínimo com 5 participante(s)</p>
								<h3>R$ 500,00</h3>
							</div>
						</div>
						<figure>
							<a href="#">
								<img src="img/home/galeria/thumb/thumb.jpg" alt="">
							</a>
						</figure>
					</div>

					<div class="entry">
						<div class="hover">
							<div>
								<span>Rapel no Salto Sete</span>
								<p>Rapel em uma excitante queda de 50 metro. Pacote mínimo com 5 participante(s)</p>
								<h3>R$ 500,00</h3>
							</div>
						</div>
						<figure>
							<a href="#">
								<img src="img/home/galeria/thumb/thumb.jpg" alt="">
							</a>
						</figure>
					</div>

					<div class="entry">
						<div class="hover">
							<div>
								<span>Rapel no Salto Sete</span>
								<p>Rapel em uma excitante queda de 50 metro. Pacote mínimo com 5 participante(s)</p>
								<h3>R$ 500,00</h3>
							</div>
						</div>
						<figure>
							<a href="#">
								<img src="img/home/galeria/thumb/thumb.jpg" alt="">
							</a>
						</figure>
					</div>

					<div class="entry">
						<div class="hover">
							<div>
								<span>Rapel no Salto Sete</span>
								<p>Rapel em uma excitante queda de 50 metro. Pacote mínimo com 5 participante(s)</p>
								<h3>R$ 500,00</h3>
							</div>
						</div>
						<figure>
							<a href="#">
								<img src="img/home/galeria/thumb/thumb.jpg" alt="">
							</a>
						</figure>
					</div>

					<div class="entry">
						<div class="hover">
							<div>
								<span>Rapel no Salto Sete</span>
								<p>Rapel em uma excitante queda de 50 metro. Pacote mínimo com 5 participante(s)</p>
								<h3>R$ 500,00</h3>
							</div>
						</div>
						<figure>
							<a href="#">
								<img src="img/home/galeria/thumb/thumb.jpg" alt="">
							</a>
						</figure>
					</div>

					<div class="entry">
						<div class="hover">
							<div>
								<span>Rapel no Salto Sete</span>
								<p>Rapel em uma excitante queda de 50 metro. Pacote mínimo com 5 participante(s)</p>
								<h3>R$ 500,00</h3>
							</div>
						</div>
						<figure>
							<a href="#">
								<img src="img/home/galeria/thumb/thumb.jpg" alt="">
							</a>
						</figure>
					</div>

					<div class="entry">
						<div class="hover">
							<div>
								<span>Rapel no Salto Sete</span>
								<p>Rapel em uma excitante queda de 50 metro. Pacote mínimo com 5 participante(s)</p>
								<h3>R$ 500,00</h3>
							</div>
						</div>
						<figure>
							<a href="#">
								<img src="img/home/galeria/thumb/thumb.jpg" alt="">
							</a>
						</figure>
					</div>
					

				</div>
				
			</article>

			<article class="news">
				<div class="title">
					<h1>Notícias</h1>
					<div class="mais">
						Confira aqui as últimas notícias do Ninho do Corvo!<br>
						<a href="noticias.php">Veja mais em nosso blog!</a>
					</div>
					<div class="arrow">
						<a href="noticias.php">
							<img src="img/home/arrow.png" alt="">
						</a>
					</div>
				</div>
				<div class="blog">

					<div class="post">
						<div class="hover">
							<a href="noticias.php">
								<img src="img/home/hover-news.png" alt="Ler Mais" title="Ler Mais"> 
							</a>
						</div>
						<img src="img/home/normal-news.jpg" alt="Noticias" title="Noticias">
						<a href="noticias.php">
							<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et rhoncus ante. Sed ultricies convallis dolor in feugiat. Donec mollis nunc quis lorem ultrices cursus. Vivamus vel magna nunc volutpat.</p>
						</a>
					</div>

					<div class="post">
						<div class="hover">
							<a href="noticias.php">
								<img src="img/home/hover-news.png" alt="Ler Mais" title="Ler Mais"> 
							</a>
						</div>
						<img src="img/home/normal-news.jpg" alt="Noticias" title="Noticias">
						<a href="noticias.php">
							<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et rhoncus ante. Sed ultricies convallis dolor in feugiat. Donec mollis nunc quis lorem ultrices cursus. Vivamus vel magna nunc volutpat.</p>
						</a>
					</div>

					<div class="post">
						<div class="hover">
							<a href="noticias.php">
								<img src="img/home/hover-news.png" alt="Ler Mais" title="Ler Mais"> 
							</a>
						</div>
						<img src="img/home/normal-news.jpg" alt="Noticias" title="Noticias">
						<a href="noticias.php">
							<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et rhoncus ante. Sed ultricies convallis dolor in feugiat. Donec mollis nunc quis lorem ultrices cursus. Vivamus vel magna nunc volutpat.</p>
						</a>
					</div>

					<div class="post">
						<div class="hover">
							<a href="noticias.php">
								<img src="img/home/hover-news.png" alt="Ler Mais" title="Ler Mais"> 
							</a>
						</div>
						<img src="img/home/normal-news.jpg" alt="Noticias" title="Noticias">
						<a href="noticias.php">
							<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et rhoncus ante. Sed ultricies convallis dolor in feugiat. Donec mollis nunc quis lorem ultrices cursus. Vivamus vel magna nunc volutpat.</p>
						</a>
					</div>
					
				</div>
			</article>

			<article class="photo">
				<h1>Galeria de Imagens</h1>
				<a href="#">Veja Galeria Completa!</a>
				<div class="thumbs">

					<div class="photos">

						<ul id="mycarousel" class="jcarousel-skin-tango">
							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/home/hover-galery.jpg" alt="">
										</a>
									</div>
									<img src="img/home/normal-galery.jpg" alt="">
								</div>
							</li>

							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/home/hover-galery.jpg" alt="">
										</a>
									</div>
									<img src="img/home/normal-galery.jpg" alt="">
								</div>
							</li>

							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/home/hover-galery.jpg" alt="">
										</a>
									</div>
									<img src="img/home/normal-galery.jpg" alt="">
								</div>
							</li>

							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/home/hover-galery.jpg" alt="">
										</a>
									</div>
									<img src="img/home/normal-galery.jpg" alt="">
								</div>
							</li>
						</ul>

					</div>

				</div>
			</article>

			<article class="depo">
				<h1>Quem já viu diz:</h1>
				<ul>
					<li><a href="#" style="color:#377852;">Veja Todos os depoimentos!</a></li>
					<li><a href="#" style="color:#864e17;">Deixe o seu!</a></li>
				</ul>
				<div class="texto">
					<span>aspas</span>
					<span>aspas</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec et rhoncus ante. Sed ultricies convallis dolor in feugiat. Donec mollis nunc quis lorem ultrices cursus. Vivamus vel magna nunc volutpat.a</p>
					<div class="credito">
						Leonardo - SP
					</div>
				</div>
			</article>

			
<?php include 'footer.php';?>