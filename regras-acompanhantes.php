<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
	<script src="js/jquery.mCustomScrollbar.min.js"></script>
	<script>
	$(document).ready(function(){
		$(".teste").mCustomScrollbar();
	});
	</script>
</head>
<style>


*
{
	margin: 0;
	padding: 0;
}

body
{
	background: #e0d8bf;
}

.intro-contato
{
	width: 920px;
}

.intro-contato h1
{
	width: 920px;
	height: auto;
	text-align: center;
	color: #377852;
	font-size: 50px;
	margin: 20px 0 30px 0;
	font-family: 'Oregano', cursive;
	text-shadow: 1px 1px 0px rgba(255,255,255,1);
}

.formulario-campos
{
	width: 860px;
	height: 390px;
	font-family: Tahoma;
	font-size: 12px;
	color: #4c4c4c;
	text-align: left;
	margin: 40px auto 0 auto;
	position: relative;
	overflow: scroll;
	padding-right: 20px;
}

.formulario-campos p
{
	float: left;
	margin-bottom: 20px;
}


</style>
<body>
	
	<div style="float: left; width: 920px; height: 550px;">
		<div class="intro-contato">
			<h1>Regras de Hospedagem</h1>
		</div>
		<div class="formulario-campos">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam interdum eros enim, nec hendrerit augue porta vel. Curabitur congue eget velit eu vehicula. Donec quis magna vitae quam facilisis malesuada eu adipiscing elit. Donec aliquam, lectus et vulputate hendrerit, tellus sem faucibus sem, id laoreet nunc sapien at est. Pellentesque vitae massa ut mauris lobortis porta. Etiam non pellentesque lectus. In laoreet bibendum malesuada. Morbi suscipit, tortor sed dictum dignissim, nulla eros viverra diam, sed consectetur felis augue sed odio.</p>
			<p>Mauris porta rutrum faucibus. Donec hendrerit eleifend turpis vel sagittis. Vestibulum sed tortor placerat, feugiat augue et, pellentesque nibh. Sed ac fermentum lectus. Nulla sollicitudin, lacus eget commodo adipiscing, metus lacus dictum enim, eu eleifend libero turpis quis lacus. Sed ligula nunc, dignissim sit amet tellus non, faucibus luctus nisl. Quisque imperdiet adipiscing justo in ultricies. Cras ac elementum lacus, non hendrerit sem. Sed vehicula urna est, ut volutpat nisl rhoncus at. Phasellus nisi felis, aliquet ut facilisis vel, ultricies sed velit.</p>
			<p>Morbi orci lacus, interdum a orci sed, pellentesque facilisis odio. Integer est sem, scelerisque vestibulum risus quis, lacinia porta quam. Nullam vehicula nulla justo, eu hendrerit libero condimentum ac. Phasellus eu consequat nisi. Proin tristique vulputate sapien in euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris semper, lacus vel vestibulum volutpat, magna libero fermentum mi, in tincidunt elit nunc pharetra erat. Proin rhoncus diam enim, quis convallis eros sagittis nec. Nunc neque augue, rutrum nec sodales vel, consequat quis odio. Maecenas sit amet condimentum lacus. Suspendisse sodales pulvinar felis eget consequat. Maecenas convallis arcu nec sagittis condimentum. Nunc vitae accumsan sem.</p>
			<p>Ut sed accumsan nunc, non posuere justo. Proin urna nisi, venenatis eget molestie et, adipiscing non justo. Etiam ut quam in nisi tempus eleifend. Nullam sed lacus tempus, gravida est a, mollis ante. Praesent non hendrerit nibh. Donec sed nunc quam. Duis congue leo diam, ac ultricies velit euismod at. Cras gravida velit eu eros volutpat, vel vulputate enim ullamcorper. Morbi faucibus diam eget metus tempus, id ornare neque venenatis. In tincidunt posuere lectus ullamcorper tincidunt. Sed mollis tincidunt ante sed volutpat. Morbi sed sem sit amet urna ultricies condimentum.</p>
			<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam interdum eros enim, nec hendrerit augue porta vel. Curabitur congue eget velit eu vehicula. Donec quis magna vitae quam facilisis malesuada eu adipiscing elit. Donec aliquam, lectus et vulputate hendrerit, tellus sem faucibus sem, id laoreet nunc sapien at est. Pellentesque vitae massa ut mauris lobortis porta. Etiam non pellentesque lectus. In laoreet bibendum malesuada. Morbi suscipit, tortor sed dictum dignissim, nulla eros viverra diam, sed consectetur felis augue sed odio.</p>
			<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam interdum eros enim, nec hendrerit augue porta vel. Curabitur congue eget velit eu vehicula. Donec quis magna vitae quam facilisis malesuada eu adipiscing elit. Donec aliquam, lectus et vulputate hendrerit, tellus sem faucibus sem, id laoreet nunc sapien at est. Pellentesque vitae massa ut mauris lobortis porta. Etiam non pellentesque lectus. In laoreet bibendum malesuada. Morbi suscipit, tortor sed dictum dignissim, nulla eros viverra diam, sed consectetur felis augue sed odio.</p>
			<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam interdum eros enim, nec hendrerit augue porta vel. Curabitur congue eget velit eu vehicula. Donec quis magna vitae quam facilisis malesuada eu adipiscing elit. Donec aliquam, lectus et vulputate hendrerit, tellus sem faucibus sem, id laoreet nunc sapien at est. Pellentesque vitae massa ut mauris lobortis porta. Etiam non pellentesque lectus. In laoreet bibendum malesuada. Morbi suscipit, tortor sed dictum dignissim, nulla eros viverra diam, sed consectetur felis augue sed odio.</p>
		

		</div>
	</div>

</body>
</html>