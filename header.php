<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<link rel="stylesheet" href="css/reset.css">
	<link href='http://fonts.googleapis.com/css?family=Oregano:400,400italic' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="css/media.css">
	<link rel="stylesheet" href="css/ad-gallery/jquery.ad-gallery.css">
	<link rel="stylesheet" href="css/jquery.mCustomScrollbar.css">
	<link rel="stylesheet" href="css/tango/skin.css">
	<link rel="stylesheet" href="css/fancy/jquery.fancybox.css">
	<link rel="stylesheet" href="css/jpreloader.css">

	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script src="js/jpreloader.min.js"></script>


	<!--<script src="js/load.js"></script>-->
	<script src="js/form.js"></script>
	<script src="js/slides.min.jquery.js"></script>
	<script src="js/jquery.jcarousel.min.js"></script>
	<script src="js/jquery.mCustomScrollbar.min.js"></script>
	<script src="js/jquery.mousewheel.min.js"></script>
	<script src="js/jquery.fancybox.pack.js"></script>
	<script src="js/jquery.fancybox-media.js"></script>

	
	<script src="js/html5shiv.js"></script>
	<script src="js/jquery.ad-gallery.min.js"></script>
	<script>
	$(window).ready(function() {
    	$('body').jpreLoader();
    });
	</script>

	<title>Ninho do Corvo</title>
</head>
<body>

	<header id="topo">
		<article class="acesso">
			<article>
				<nav class="menu-acesso">
					<ul>
						<li>
							<a href="#" class="acesso">
								<img src="img/header/cadeado.png" alt="Login" title="Login">
								Login
							</a>
							<div class="login">
								<form action="minha-conta.php">
									<fieldset>
										<label for="login">
											<span>Login</span>
											<input type="text" name="login" id="login">
										</label>
										<label for="password">
											<span>Senha</span>
											<input type="password" name="password" id="password">
										</label>
										<input type="submit" name="logar" value="login">
										<ul>
											<li><input type="checkbox" value="lembrar" class="styled"> Lembrar de mim</li>
											<li><a href="#">Esqueci meu login</a></li>
											<li><a href="#">Esqueci minha senha</a></li>
										</ul>
									</fieldset>
								</form>
							</div>
						</li>
						<li>
							<a href="cadastro.php">
								<img src="img/header/cadastre-se.png" alt="Cadastre-se" title="Cadastre-se">
								Cadastre-se
							</a>
						</li>
						<li>
							<a href="minha-conta.php">
								<img src="img/header/login.png" alt="Minha Conta" title="Minha Conta">
								Minha Conta
							</a>
						</li>
					</ul>
				</nav>
				<div class="carrinho">
					<a href="checkout.php">
						<span>300</span>
					</a>
				</div>
				<div class="search">
					<form action="busca.php">
						<fieldset>
							<label for="buscar">
								<input type="text" name="buscar" value="" id="buscar">
							</label>
							<input type="submit" name="submit">
						</fieldset>
					</form>
				</div>
				<div class="menu">
					<nav>
						<ul>
							<li style="margin-top: 20px;"><a href="oninho.php">O Ninho</a></li>
							<li style="margin-top: 20px;"><a href="rppn.php">RPPN</a></li>
							<li style="margin-top: 20px;"><a href="atividades.php">Atividades</a></li>
							<li style="margin-top: 20px;"><a href="hospedagem.php">Hospedagem</a></li>
							<li style="margin-top: -45px;"><a href="index.php"><img src="img/header/logo.png" alt="Ninho do Corvo" title="Ninho do Corvo"></a></li>
							<li style="margin-top: 10px;"><a href="perguntas.php">Perguntas<br>Frequentes</a></li>
							<li style="margin-top: 10px;"><a href="turismo-ativo.php">Turismo<br>Ativo</a></li>
							<li style="margin-top: 20px;"><a href="mural.php">Mural</a></li>
							<li style="margin-top: 20px;"><a href="contato.php">Contato</a></li>
						</ul>
					</nav>
				</div>
			</article>
		</article>
	</header>