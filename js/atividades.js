$(document).ready(function(){
	$('#content .middle .galeria-atividades .menu .entry').hover(function(){
				$(this).children('.hover').fadeIn(200);
			}, function(){
				$(this).children('.hover').fadeOut(200);
		});

	$( ".tabs" ).tabs({
			show:{
				effect: "fade", 
				duration: 150 
			}, 
			hide:{ 
				effect: "fade", 
				duration: 150 
			}
		});

	$(".regras").fancybox({
			scrolling: "no",
			preload: false,
			href: "regras-hospedagem.php",
			type: "iframe",
			width: '920px',
      		height: '550px',
      		fitToView : false,
   			autoSize : false
		});

	$(".regras-acompanhantes").fancybox({
			scrolling: "no",
			preload: false,
			href: "regras-acompanhantes.php",
			type: "iframe",
			width: '920px',
      		height: '550px',
      		fitToView : false,
   			autoSize : false
		});

	$(".regras-cardapio").fancybox({
			scrolling: "no",
			preload: false,
			href: "cardapio.php",
			type: "iframe",
			width: '920px',
      		height: '300px',
      		fitToView : false,
   			autoSize : false
		})
});
