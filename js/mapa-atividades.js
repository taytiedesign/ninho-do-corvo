var directionDisplay;
        var directionsService = new google.maps.DirectionsService();
        var map;
 
        function initialize() {

            directionsDisplay = new google.maps.DirectionsRenderer();
            var myLatlng = new google.maps.LatLng();
             
            var myOptions = {
                zoom:4,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                center: myLatlng,
                center: new google.maps.LatLng(-24.846565,-51.240234)
            }
 
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById("directionsPanel"));

           
        }
        /*
        function calcRoute() {
            var start = document.getElementById("endereco").value;
            var end = document.getElementById("destino").value;
            var request = {
                origin:start, 
                destination:end,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
             
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                } else {
                    alert(status);
                }
 
                document.getElementById('mapview').style.visibility = 'visible';
            });
        }
        */
        google.maps.event.addDomListener(window, 'load', initialize);