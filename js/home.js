$(document).ready(function(){

	$('#content .middle .news .blog .post').hover(function(){
				$(this).children('.hover').fadeIn(200);
			}, function(){
				$(this).children('.hover').fadeOut(200);
		});

		$('#content .middle .photo .thumbs .photos .item').hover(function(){
				$(this).children('.hover').fadeIn(200);
			}, function(){
				$(this).children('.hover').fadeOut(200);
		});

		$('#content .middle .galeria .entry').hover(function(){
				$(this).children('.hover').fadeIn(200);
			}, function(){
				$(this).children('.hover').fadeOut(200);
		});

		$('.fancybox').fancybox();

		$('.fancybox2')
				.attr('rel', 'media-gallery')
				.fancybox({
					arrows : true,
					helpers : {
						media : {},
						buttons : {}
					}
				});

		$(".content, .content-ninho, .content-hospedagem").mCustomScrollbar({
		    horizontalScroll: true,
		    theme: "light-thick"
		});

		$('#mycarousel').jcarousel();

		// Ao passa o Mouse ele abre

		$('#topo .acesso article .menu-acesso ul li a.acesso').mouseenter(function(){
			$('#topo .acesso article .menu-acesso ul li .login').fadeIn(200);
		});

		// Ao sair com o Mouse ele fecha, quando sair do elemento .login

		$('#topo .acesso article .menu-acesso ul li .login').mouseleave(function(){
			$(this).fadeOut(200);
		});

		console.log('teste');

});