$(document).ready(function(){
	$('#content .middle .todas-fotos-mural .entry').hover(function(){
				$(this).children('.hover').fadeIn(200);
			}, function(){
				$(this).children('.hover').fadeOut(200);
		});

		$('#content .middle .mural-depoimento-fotos .registros-fotos .item').hover(function(){
				$(this).children('.hover').fadeIn(200);
			}, function(){
				$(this).children('.hover').fadeOut(200);
		});	

		$('#content .middle .mural-depoimento-videos .registros-videos .item').hover(function(){
				$(this).children('.hover').fadeIn(200);
			}, function(){
				$(this).children('.hover').fadeOut(200);
		});


		$(".envie").fancybox({
			scrolling: "no",
			preload: false,
			href: "envia-mural.php",
			type: "iframe",
			width: '920px',
      		height: '460px',
      		fitToView : false,
   			autoSize : false
		});

		$('#mycarousel2').jcarousel();
});
