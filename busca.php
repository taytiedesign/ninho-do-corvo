<?php include 'header.php';?>
	<section id="content" class="noticias">
		<!-- MATAGAL -->
		<article class="fotos-noticias">
			<figure>
				<img class="lazy" src="img/noticias/fotos.png" alt="">
			</figure>
		</article>
		<article class="capaceteRight">
			<figure>
				<img class="lazy" src="img/noticias/capacete.png" alt="">
			</figure>
		</article>
		<article class="bike-noticias">
			<figure>
				<img class="lazy" src="img/noticias/bike.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/mural/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-contato">
				<h1>Busca</h1>
			</div>
			
			<div class="registro">
				<div class="noticias">
					
					<div class="resultado-busca">
						Resultados encontrados para: <span>Jun / 2013</span>
					</div>

					<div class="secundarias">
						<a href="noticias-interna.php">
							<img src="img/noticias/foto.jpg" alt="">
						</a>
						<div>
							<h1>
								Canoagem alia aventura e apreciação da natureza
							</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in dui vulputate augue laoreet ullamcorper. Mauris ipsum metus, varius tincidunt molestie at, tincidunt sed est. Nulla diam dui, scelerisque a commodo vitae, placerat vel felis. Aenean egestas, elit vitae convallis convallis, libero nibh malesuada felis, sed adipiscing enim odio id odio. Morbi quis arcu felis. In ornare sem a condimentum laoreet. Praesent id fermentum est. Aliquam sollicitudin vel leo et pharetra. Donec eget volutpat... <a href="noticias-interna.php">Veja Mais</a></p>
						</div>
					</div>

					<div class="secundarias">
						<a href="noticias-interna.php">
							<img src="img/noticias/foto.jpg" alt="">
						</a>
						<div>
							<h1>
								Canoagem alia aventura e apreciação da natureza
							</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in dui vulputate augue laoreet ullamcorper. Mauris ipsum metus, varius tincidunt molestie at, tincidunt sed est. Nulla diam dui, scelerisque a commodo vitae, placerat vel felis. Aenean egestas, elit vitae convallis convallis, libero nibh malesuada felis, sed adipiscing enim odio id odio. Morbi quis arcu felis. In ornare sem a condimentum laoreet. Praesent id fermentum est. Aliquam sollicitudin vel leo et pharetra. Donec eget volutpat... <a href="noticias-interna.php">Veja Mais</a></p>
						</div>
					</div>

					<div class="secundarias">
						<a href="noticias-interna.php">
							<img src="img/noticias/foto.jpg" alt="">
						</a>
						<div>
							<h1>
								Canoagem alia aventura e apreciação da natureza
							</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in dui vulputate augue laoreet ullamcorper. Mauris ipsum metus, varius tincidunt molestie at, tincidunt sed est. Nulla diam dui, scelerisque a commodo vitae, placerat vel felis. Aenean egestas, elit vitae convallis convallis, libero nibh malesuada felis, sed adipiscing enim odio id odio. Morbi quis arcu felis. In ornare sem a condimentum laoreet. Praesent id fermentum est. Aliquam sollicitudin vel leo et pharetra. Donec eget volutpat... <a href="noticias-interna.php">Veja Mais</a></p>
						</div>
					</div>

					<div class="secundarias">
						<a href="noticias-interna.php">
							<img src="img/noticias/foto.jpg" alt="">
						</a>
						<div>
							<h1>
								Canoagem alia aventura e apreciação da natureza
							</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in dui vulputate augue laoreet ullamcorper. Mauris ipsum metus, varius tincidunt molestie at, tincidunt sed est. Nulla diam dui, scelerisque a commodo vitae, placerat vel felis. Aenean egestas, elit vitae convallis convallis, libero nibh malesuada felis, sed adipiscing enim odio id odio. Morbi quis arcu felis. In ornare sem a condimentum laoreet. Praesent id fermentum est. Aliquam sollicitudin vel leo et pharetra. Donec eget volutpat... <a href="noticias-interna.php">Veja Mais</a></p>
						</div>
					</div>

					<div class="secundarias">
						<a href="noticias-interna.php">
							<img src="img/noticias/foto.jpg" alt="">
						</a>
						<div>
							<h1>
								Canoagem alia aventura e apreciação da natureza
							</h1>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis in dui vulputate augue laoreet ullamcorper. Mauris ipsum metus, varius tincidunt molestie at, tincidunt sed est. Nulla diam dui, scelerisque a commodo vitae, placerat vel felis. Aenean egestas, elit vitae convallis convallis, libero nibh malesuada felis, sed adipiscing enim odio id odio. Morbi quis arcu felis. In ornare sem a condimentum laoreet. Praesent id fermentum est. Aliquam sollicitudin vel leo et pharetra. Donec eget volutpat... <a href="noticias-interna.php">Veja Mais</a></p>
						</div>
					</div>

					<div class="page">
						<ul>
							<li>
								<a href="#" class="selected">1</a>
							</li>
							<li>
								<a href="#">2</a>
							</li>
							<li>
								<a href="#">3</a>
							</li>
							<li>
								<a href="#">4</a>
							</li>
						</ul>
					</div>

				</div>
				<div class="sidebar">
					<div class="tags">
						<h1>Tags</h1>
						palavras de tags, os tamanhos varia de acordo com palavras cadastradas e também no back-end
					</div>
					<div class="arquivos">
						<h1>Arquivo</h1>
						<select name="arquivo" id="arquivo" class="styled">
							<option value="0">Selecione um periodo</option>
							<option value="1">2013</option>
							<option value="1">2012</option>
							<option value="1">2011</option>
							<option value="1">2010</option>
						</select>
						<input type="submit" name="filtra-arquivo">
					</div>
					<div class="mais-lidos">
						<h1>Mais Lidos</h1>
						<div>
							<a href="#">
								<img src="img/noticias/mais.jpg" alt="">
							</a>
							<strong>Canoagem alia aventura e apreciação da natureza</strong>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor, ipsum eget aliquam massa nunc.</p>
						</div>

						<div>
							<a href="#">
								<img src="img/noticias/mais.jpg" alt="">
							</a>
							<strong>Canoagem alia aventura e apreciação da natureza</strong>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor, ipsum eget aliquam massa nunc.</p>
						</div>

						<div>
							<a href="#">
								<img src="img/noticias/mais.jpg" alt="">
							</a>
							<strong>Canoagem alia aventura e apreciação da natureza</strong>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor, ipsum eget aliquam massa nunc.</p>
						</div>

						<div>
							<a href="#">
								<img src="img/noticias/mais.jpg" alt="">
							</a>
							<strong>Canoagem alia aventura e apreciação da natureza</strong>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor, ipsum eget aliquam massa nunc.</p>
						</div>
					</div>
				</div>
			</div>
			
			
<?php include 'footer.php';?>