<?php include 'header.php';?>
	<section id="content" class="oninho">
		<!-- MATAGAL -->
		<article class="ave">
			<figure>
				<img src="img/o-ninho/Ave.png" alt="">
			</figure>
		</article>
		<article class="esquilo">
			<figure>
				<img class="lazy" src="img/o-ninho/esquilo.png" alt="">
			</figure>
		</article>

		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->
		<article class="banner-interno">
			<img src="img/o-ninho/topo.png" alt="">
		</article>
		<article class="middle clearfix">
			<div class="intro-o-ninho">
				<h1>O Ninho do Corvo</h1>
				<div class="frase">
					Seja bem vindo ao nosso parque e desfrute das atrações<br>desenvolvidas junto a natureza, num cenário de grande beleza.
				</div>
				<div class="texto">
					<p>O Ninho do Corvo está localizado no interior da cidade de Prudentópolis e possui uma área total de 18,50 hectares, sendo que destes 10,55 foram transformados em RPPN (Reserva Particular do Patrimônio Natural), constituindo uma área de preservação permanente. Existem  8 quedas d´água dentro da área sendo que algumas estão abertas a visitação publica e outras somente com visitas agendadas e guiadas.</p>
					<p>O local  foi adquirido em 2002 com a intenção de seus proprietários transformarem a área , que era utilizada para a lavoura de feijão e milho na maior parte, em um pequeno empreendimento de eco-turismo, que trabalha respeitando o ambiente e a comunidade em geral,  procurando valorizar a mão de obra local e passar os valores do conservacionismo e do ecoturismo responsável.</p>
					<p>Como atrações, além das atividades verticais, o local conta com mais de 2.000 metros de trilhas auto-guiadas que levam o visitante a diversos pontos de grande interesse como cachoeiras, mirantes, rios, alem do belo cânion do rio Barra Bonita e seu poço na parte superior onde é possível banhar-se.
Outros pontos a visitação acontece apenas com o acompanhamento de guias e passeios que podem ser agendados.</p>
				</div>
				<div class="texto">
					<p>O Ninho do Corvo é uma empresa registrada no Ministério do Turismo, associada a ABETA – Assoc. Brasileira das Empresas de Ecoturismo e Aventura, cooperada a COOPTUR – Cooperativa Paranaense de Turismo e parceira da ECOTRIP empresa de seguros contra acidentes.</p>
					<p>Nossa equipe é composta de pessoal treinado e plenamente capacitado para que a atividade seja desenvolvida com toda a segurança e satisfação possível por nossos clientes. Utilizamos somente equipamentos certificados pelos órgãos competentes a nível nacional e mundial. (CE, UIAA). Nossos aparelhos passam por constante manutenção e atualização  sempre pensando na segurança.</p>
					<p>Passar um dia no Ninho do Corvo é passeio para toda a família desde  avós até os netos. Nossas atividades foram desenvolvidas para oferecer emoção e segurança tanto para as pessoas que nunca tiveram contato com este tipo atividade quanto para aqueles que já tem alguma experiência, podendo ser praticada por toda a família junta.</p>
				</div>
			</div>
		<article class="galeria-o-ninho">
			<h3>Entre em contato conosco e venha passar um dia se divertindo no Ninho!</h3>
			<div class="content-ninho">
				<div class="entry">
					<div class="hover">
						<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
							<img src="img/o-ninho/hover.png" alt="">
						</a>
					</div>
					<img src="img/o-ninho/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
							<img src="img/o-ninho/hover.png" alt="">
						</a>
					</div>
					<img src="img/o-ninho/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
							<img src="img/o-ninho/hover.png" alt="">
						</a>
					</div>
					<img src="img/o-ninho/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
							<img src="img/o-ninho/hover.png" alt="">
						</a>
					</div>
					<img src="img/o-ninho/foto.jpg" alt="">
				</div>
				<div class="entry">
					<div class="hover">
						<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
							<img src="img/o-ninho/hover.png" alt="">
						</a>
					</div>
					<img src="img/o-ninho/foto.jpg" alt="">
				</div>
			</div>
		</article>

			
<?php include 'footer.php';?>