<?php include 'header.php';?>
	<section id="content" class="cadastro">
		<!-- MATAGAL -->
		<article class="ave-contato">
			<figure>
				<img class="lazy" src="img/cadastro/ave.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/cadastro/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-minha-conta">
				<h1>Área do Usuário</h1>
				<div class="saudacao">
					<ul>
						<li>Bem Vindo (a)</li>
						<li>Leonardo Taytie</li>
					</ul>
				</div>
			</div>

			<div class="minha-conta">
				<div class="change">
					<ul>
						<li>
							<a href="minha-conta.php" class="selected">
								Alterar Dados de Cadastro
							</a>
						</li>
						<li>
							<a href="meus-pedidos.php">
								Meus Pedidos
							</a>
						</li>
						<li>
							<a href="index.php">
								Sair
							</a>
						</li>
					</ul>
				</div>
				<div class="formulario-minha-conta">
					<form action="#">
						<fieldset>
							<label for="nome">
								<span>Nome:</span>
								<input type="text" name="nome">
							</label>
							<label for="email">
								<span>E-mail:</span>
								<input type="text" name="email">
							</label>
							<label for="end">
								<span>E-mail:</span>
								<input type="text" name="end">
							</label>
							<label for="cep">
								<span>CEP:</span>
								<input type="text" name="cep">
							</label>
							<label for="bairro">
								<span>Bairro:</span>
								<input type="text" name="bairro">
							</label>
							<label for="cidade">
								<span>Cidade:</span>
								<input type="text" name="cidade">
							</label>
							<label for="estado">
								<span>Estado:</span>
								<select name="estado" id="estado" class="styled">
									<option value="1">SP</option>
									<option value="2">PR</option>
								</select>
							</label>
							<label for="telefone">
								<span>Telefone:</span>
								<input type="text" name="telefone">
							</label>
							<label for="celular">
								<span>Cidade:</span>
								<input type="text" name="celular">
							</label>
							<label for="cpf">
								<span>CPF:</span>
								<input type="text" name="cpf">
							</label>
							<label for="senha">
								<span>Senha:</span>
								<input type="password" name="senha">
							</label>
							<div class="options">
								<div>
									<input type="checkbox" name="newsletter" class="styled">Desejo assinar a Newsletter do Ninho do Corvo<br>
								</div>
								<div>
									<input type="checkbox" name="termos" class="styled">Estou de acordo com os <a href="#">Termos de Uso</a> do Website da Ninho do Corvo.
								<div>
							</div>
							<input type="submit" name="enviar">
						</fieldset>
					</form>
				</div>
			</div>

			
			
<?php include 'footer.php';?>