<?php include 'header.php';?>
	<section id="content" class="contato">
		<!-- MATAGAL -->
		<article class="traking-contato">
			<figure>
				<img class="lazy" src="img/contato/trekking.png" alt="">
			</figure>
		</article>
		<article class="esquilo-contato">
			<figure>
				<img class="lazy" src="img/contato/esquilo.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/mural/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-contato">
				<h1>Contato</h1>
			</div>

			<div class="dados-contato">
				<div class="conteudo">
					<div class="texto">
						<div class="titulo">
							Linha Paraná - Prudentópolis - PR
						</div>
						<div class="end">
							<ul>
								<li>CEP 84.400-000</li>
								<li>Ninho: (42) <span>3224.0606</span> (à noite) / <span>9976.7227</span></li>
								<li>RPPN: (42) <span>9131-8694</span> / (42) <span>9113-1304</span></li>
							</ul>
						</div>
					</div>

					<div class="texto">
						<div class="titulo2">Você pode entrar em conosco através dos
telefones para informações e reservas:
						</div>
						<div class="tel">
							<ul>
								<li><img src="img/contato/tel.png" alt="">(42) <span>9131-8694</span></li>
								<li><img src="img/contato/tel.png" alt="">(42) <span>3224-0606</span> (à noite) </li>
							</ul>
						</div>
					</div>
				</div>
				<div class="formulario">
					<div>
						* Como estamos na área rural, pode ser que você tenha dificuldades em realizar a chamada nestes telefones.
Ou pode usar o formulário abaixo e entraremos em contato o mais rápido possível.
					</div>
				</div>
				<div class="formulario-campos">
					<form action="#">
						<fieldset>
							<label for="nome">
								<span>Nome:</span>
								<input type="text" name="nome" id="nome">
							</label>
							<label for="email">
								<span>E-mail:</span>
								<input type="text" name="email" id="email">
							</label>
							<label for="cidade">
								<span>Cidade:</span>
								<input type="text" name="cidade" id="cidade">
							</label>
							<label for="telefone">
								<span>Telefone:</span>
								<input type="text" name="telefone" id="telefone">
							</label>
							<label for="assunto">
								<span>Assunto:</span>
								<input type="text" name="assunto" id="assunto">
							</label>
						</fieldset>
						<fieldset>
							<label for="mensagem">
								<span>Mensagem:</span>
								<textarea name="mensagem" id="mensagem"></textarea>
							</label>
							<input type="submit" name="enviar">
						</fieldset>
					</form>
				</div>
				
			</div>

			<div class="mapa">
				<div>

					<form action="javascript: void(0);" onSubmit="calcRoute()">
					    <div>
					    	<label for="destino">Partida:</label>
					        <input type="text" size="50" value="Endereço - UF" id="destino" />
					    </div>
					     
					    <div>
					        <label for="endereco">Chegada:</label>
					        <input type="text" size="50" value="Prudentópolis - PR" id="endereco" />
					    </div>
					    <button type="submit" id="mapa">Rota</button>
					</form>
					
					<div id="mapviewContato">
					    <div id="map_canvas" style="float: left; width: 940px; height: 340px;"></div>
					    <div class="direcao" style="float: left; width: 940px; height: 340px; overflow: scroll; display:none; background: #fff; position: absolute; top: 395px; z-index: 999;">
					        <div id="directionsPanel" style="width: 940px; height; 100px; float: left; margin-top:-10px;"></div>
					    </div>
					    <div class="print-mapa">
					    	<button type="submit" onclick="printSelection(document.getElementById('directionsPanel'));return false">Imprimir Mapa</button>
					    </div>
					</div>

				</div>
			</div>


			
<?php include 'footer.php';?>