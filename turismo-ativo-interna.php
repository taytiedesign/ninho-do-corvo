<?php include 'header.php';?>
	<section id="content" class="turismo-interna">
		<!-- MATAGAL -->
		<article class="carro-turismo">
			<figure>
				<img class="lazy" src="img/turismo/carro.png" alt="">
			</figure>
		</article>
		<article class="bike-turimo">
			<figure>
				<img class="lazy" src="img/turismo/bike.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/turismo/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-turismo-interno">
				<h1>Caminhos Andinos Expedição Altiplano</h1>
			</div>

			<div class="galeria-atividades-interna">
					
				<div id="gallery" class="ad-gallery">
			      
			      <div class="ad-image-wrapper"></div>

			      <div class="ad-nav">
			        <div class="ad-thumbs">
			          <ul class="ad-thumb-list">
			            <li>
			              <a href="images/1.jpg">
			                <img src="images/thumbs/t1.jpg">
			              </a>
			            </li>
			            <li>
			              <a href="images/2.jpg">
			                <img src="images/thumbs/t2.jpg">
			              </a>
			            </li>
			            <li>
			              <a href="images/3.jpg">
			                <img src="images/thumbs/t3.jpg">
			              </a>
			            </li>
			            <li>
			              <a href="images/4.jpg">
			                <img src="images/thumbs/t4.jpg">
			              </a>
			            </li>
			            <li>
			              <a href="images/5.jpg">
			                <img src="images/thumbs/t5.jpg">
			              </a>
			            </li>  
			            <li>
			              <a href="images/6.jpg">
			                <img src="images/thumbs/t6.jpg">
			              </a>
			            </li>
			          </ul>
			        </div>
			      </div>
			    </div>

			</div>

			<div class="roteiro-turismo">
				<div class="historia">
					Roteiro em veículo de 12 dias pelos Andes Argentinos! Serão alguns dias bem intensos com muitas paisagens alucinantes, histórias, folclore e diversão!
				</div>
				<div class="info">
					<div class="tabs-turismo">
						<ul>
						    <li><a href="#turismo-1">Descrição</a></li>
						    <li><a href="#turismo-2">Roteiro</a></li>
						    <li><a href="#turismo-3">Informações</a></li>
						    <li><a href="#turismo-4">Contato</a></li>
						</ul>
						<div id="turismo-1">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis augue tellus. Nam euismod mi ut felis mollis fermentum. Aenean faucibus sem ipsum, quis adipiscing est commodo quis. Aenean tincidunt dolor id cursus ultricies. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean venenatis quis nibh eu condimentum. Nulla at venenatis lectus, a malesuada ante. Nam vel lorem sagittis, tincidunt dolor a, tempor lacus. Suspendisse potenti. Aenean eget ante id lectus rutrum pharetra. Curabitur bibendum, justo quis fermentum rhoncus, quam lacus imperdiet diam, vitae volutpat orci lacus ac nisi. Sed lectus nulla, egestas ac neque in, luctus pharetra massa. Nunc ac tortor in orci lobortis accumsan sed vitae neque.</p>
							<p>Fusce a leo ut ligula euismod congue in ut metus. Duis scelerisque ligula at nibh porttitor, sit amet elementum magna laoreet. Aenean eu interdum orci, quis aliquet mi. Fusce leo risus, rutrum id arcu ac, consequat pharetra purus. In egestas ante a sem gravida commodo. Aliquam varius sem a dui posuere mollis. Praesent dolor quam, pretium eget risus vitae, ultrices pellentesque leo. Donec nec diam ut arcu cursus blandit quis non urna. Etiam vel vulputate metus.</p>
						</div>
						<div id="turismo-2">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis augue tellus. Nam euismod mi ut felis mollis fermentum. Aenean faucibus sem ipsum, quis adipiscing est commodo quis. Aenean tincidunt dolor id cursus ultricies. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean venenatis quis nibh eu condimentum. Nulla at venenatis lectus, a malesuada ante. Nam vel lorem sagittis, tincidunt dolor a, tempor lacus. Suspendisse potenti. Aenean eget ante id lectus rutrum pharetra. Curabitur bibendum, justo quis fermentum rhoncus, quam lacus imperdiet diam, vitae volutpat orci lacus ac nisi. Sed lectus nulla, egestas ac neque in, luctus pharetra massa. Nunc ac tortor in orci lobortis accumsan sed vitae neque.</p>
						</div>
						<div id="turismo-3">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis augue tellus. Nam euismod mi ut felis mollis fermentum. Aenean faucibus sem ipsum, quis adipiscing est commodo quis. Aenean tincidunt dolor id cursus ultricies. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean venenatis quis nibh eu condimentum. Nulla at venenatis lectus, a malesuada ante. Nam vel lorem sagittis, tincidunt dolor a, tempor lacus. Suspendisse potenti. Aenean eget ante id lectus rutrum pharetra. Curabitur bibendum, justo quis fermentum rhoncus, quam lacus imperdiet diam, vitae volutpat orci lacus ac nisi. Sed lectus nulla, egestas ac neque in, luctus pharetra massa. Nunc ac tortor in orci lobortis accumsan sed vitae neque.</p>
							<p>Fusce a leo ut ligula euismod congue in ut metus. Duis scelerisque ligula at nibh porttitor, sit amet elementum magna laoreet. Aenean eu interdum orci, quis aliquet mi. Fusce leo risus, rutrum id arcu ac, consequat pharetra purus. In egestas ante a sem gravida commodo. Aliquam varius sem a dui posuere mollis. Praesent dolor quam, pretium eget risus vitae, ultrices pellentesque leo. Donec nec diam ut arcu cursus blandit quis non urna. Etiam vel vulputate metus.</p>
						</div>
						<div id="turismo-4">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis augue tellus. Nam euismod mi ut felis mollis fermentum. Aenean faucibus sem ipsum, quis adipiscing est commodo quis. Aenean tincidunt dolor id cursus ultricies. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean venenatis quis nibh eu condimentum. Nulla at venenatis lectus, a malesuada ante. Nam vel lorem sagittis, tincidunt dolor a, tempor lacus. Suspendisse potenti. Aenean eget ante id lectus rutrum pharetra. Curabitur bibendum, justo quis fermentum rhoncus, quam lacus imperdiet diam, vitae volutpat orci lacus ac nisi. Sed lectus nulla, egestas ac neque in, luctus pharetra massa. Nunc ac tortor in orci lobortis accumsan sed vitae neque.</p>
						</div>
					</div>
				</div>
			</div>







			

			

			
		

			
<?php include 'footer.php';?>