<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<style>
/*
#content .middle .dados-contato
{
	width: 940px;
	min-height: 500px;
	float: left;
}

#content .middle .dados-contato .conteudo
{
	width: 780px;
	height: auto;
	margin: 0 auto;
}

#content .middle .dados-contato .conteudo .texto
{
	width: 380px;
	min-height: 100px;
	float: left;
	margin-right: 20px;
}

#content .middle .dados-contato .conteudo .texto .end
{
	float: left;
	width: 100%;
	height: auto;
}

#content .middle .dados-contato .conteudo .texto .end ul
{
	list-style: none;
	font-family: Tahoma;
	font-size: 12px;
	color: #4c4c4c;
	margin-left: 10px;
}

#content .middle .dados-contato .conteudo .texto .end ul li
{
	margin-bottom: 05px;
}

#content .middle .dados-contato .conteudo .texto .end ul li:nth-child(1)
{
	margin-bottom: 20px;
}

#content .middle .dados-contato .conteudo .texto .end span
{
	color: #775701;
	font-weight: bold;
}

#content .middle .dados-contato .conteudo .texto .tel
{
	float: left;
	width: 100%;
	height: auto;
}

#content .middle .dados-contato .conteudo .texto .tel ul
{
	list-style: none;
	font-family: Tahoma;
	font-size: 12px;
	color: #4c4c4c;
	margin-top: 13px;
	margin-left: 10px;
}

#content .middle .dados-contato .conteudo .texto .tel ul li
{
	margin-bottom: 05px;
}

#content .middle .dados-contato .conteudo .texto .tel ul li img
{
	float: left;
	margin: 0 10px 0 0;
}

#content .middle .dados-contato .conteudo .texto .tel span
{
	color: #775701;
	font-weight: bold;
}

#content .middle .dados-contato .conteudo .texto:last-child
{
	margin-right: 0px!important;
}

#content .middle .dados-contato .conteudo .texto .titulo
{
	color: #f5f0df;
	font-size: 28px;
	line-height: 100px;
	text-align: center;
	background: url(img/contato/bg-titulo.png) no-repeat;
	text-shadow: 1px 1px 0px rgba(0,0,0,.7);
	font-family: 'Oregano', cursive;
}

#content .middle .dados-contato .conteudo .texto .titulo2
{
	color: #f5f0df;
	font-size: 24px;
	height: 100px;
	padding-top: 20px;
	text-align: center;
	background: url(img/contato/bg-titulo.png) no-repeat;
	text-shadow: 1px 1px 0px rgba(0,0,0,.7);
	font-family: 'Oregano', cursive;
}

#content .middle .dados-contato .formulario
{
	float: left;
	width: 940px;
	font-family: Tahoma;
	font-size: 12px;
	color: #4c4c4c;
	text-align: center;
	margin-top: 10px;
}

#content .middle .dados-contato .formulario div
{
	width: 620px;
	height: auto;
	margin: 0 auto;
}

*/


*
{
	margin: 0;
	padding: 0;
}

body
{
	background: #e0d8bf;
}

.intro-contato
{
	width: 920px;
}

.intro-contato h1
{
	width: 920px;
	height: auto;
	text-align: center;
	color: #377852;
	font-size: 50px;
	margin: 20px 0 30px 0;
	font-family: 'Oregano', cursive;
	text-shadow: 1px 1px 0px rgba(255,255,255,1);
}

.formulario-campos
{
	float: left;
	width: 920px;
	height: 290px;
	font-family: Tahoma;
	font-size: 12px;
	color: #4c4c4c;
	text-align: center;
	margin-top: 40px;
}

.formulario-campos form
{
	float: left;
}

.formulario-campos form fieldset
{
	border: none;
}

.formulario-campos form fieldset:nth-child(1)
{
	float: left;
	width: 430px;
	margin-left: 50px;
}

.formulario-campos form fieldset:nth-child(2)
{
	float: right!important;
	width: 430px;
	margin-left: 0px!important;
}

.formulario-campos form fieldset label
{
	float: left;
	width: auto;
	margin-bottom: 15px;
}

.formulario-campos form fieldset label span
{
	float: left;
	width: 100px;
	line-height: 30px;
	text-align: right;
	margin-right: 10px;
}

.formulario-campos form fieldset label input[type="text"]
{
	float: right;
	width: 250px;
	height: 30px;
	border: none;
	color: #4c4c4c;
	background: #c5bfa9;
	border-radius: 05px;
	box-shadow: 0px 0px 05px rgba(255,255,255,.5), inset 1px 1px 02px rgba(0,0,0,.5);
	padding: 0 10px 0 10px;
}

.formulario-campos form fieldset label textarea
{
	float: right;
	width: 238px!important;
	height: 200px!important;
	border: none;
	color: #4c4c4c;
	background: #c5bfa9;
	border-radius: 05px;
	box-shadow: 0px 0px 05px rgba(255,255,255,.5), inset 1px 1px 02px rgba(0,0,0,.5);
	padding: 10px 10px 0 10px;
}

.formulario-campos form fieldset input[type="submit"]
{
	width: 260px;
	height: 37px;
	background: url(img/contato/btn_enviar_normal.png) no-repeat;
	float: right;
	border: none;
	margin-right: 60px;
	text-indent: -999999px;
	cursor: pointer;
}

.formulario-campos form fieldset input[type="submit"]:hover
{
	background: url(img/contato/btn_enviar_hover.png) no-repeat;
}
</style>
<body>
	
	<div style="float: left; width: 920px; height: 460px;">
		<div class="intro-contato">
			<h1>Envio de depoimento, imagens e vídeos</h1>
		</div>
		<div class="formulario-campos">
			<form action="#">
				<fieldset>
					<label for="nome">
						<span>Nome:</span>
						<input type="text" name="nome" id="nome">
					</label>
					<label for="email">
						<span>E-mail:</span>
						<input type="text" name="email" id="email">
					</label>
					<label for="cidade">
						<span>Cidade:</span>
						<input type="text" name="cidade" id="cidade">
					</label>
					<label for="links">
						<span>Links para Videos:</span>
						<input type="text" name="links" id="links">
						<div style="float: rigth; margin-top: 10px;">** Separe os links com vírgulas</div>
					</label>
					<label for="upload">
						<span>Enviar Imagens:</span>
						<input type="file" name="upload" id="upload">
					</label>
				</fieldset>
				<fieldset>
					<label for="mensagem">
						<span>Mensagem:</span>
						<textarea name="mensagem" id="mensagem"></textarea>
					</label>
					<input type="submit" name="enviar">
				</fieldset>
			</form>
		</div>
	</div>

</body>
</html>