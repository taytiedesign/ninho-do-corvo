	<article class="novidades clearfix">
				<div class="newsletter">
					<div class="title">
						<h3>Novidades do Ninho</h3>
						<ul>
							<li><a href="#"><img src="img/home/facebook.png" alt="Facebook" title="Facebook"></a></li>
							<li><a href="#"><img src="img/home/twitter.png" alt="Twitter" title="Twitter"></a></li>
							<li>Acompanhe nossas redes sociais!</li>
						</ul>
					</div>
					<div class="formulario">
						<p>Assinhe nossa newsletter e receba em primeira mão nossas novidades e promoções</p>
						<form action="#">
							<fieldset>
								<img src="img/home/news.png" alt="">
								<ul>
									<li>
										<label for="email">
											News
										</label>
									</li>
									<li>
										<input type="text" id="email" name="email">
										<input type="submit" name="enviar">
									</li>
								</ul>
							</fieldset>
						</form>
					</div>
				</div>
				<div class="facebook clearfix">
					<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FRPPN-Ninho-do-Corvo%2F181107803395%3Ffref%3Dts&amp;width=500&amp;height=220&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=false&amp;appId=162089477190966" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:500px; height:220px;" allowTransparency="true"></iframe>
				</div>
			</article>
		</article>
		
	</section>
	<footer id="footer">
		<aside>
			<div class="info">
				<ul>
					<li><a href="#">Regras de negócio</a></li>
					<li><a href="#">Como Comprar</a></li>
				</ul>
			</div>
			<div class="copy">
				<ul>
					<li>© Ninho do Corvo - Todos os direitos reservados</li>
					<li>
						<a href="#">
							Desenvolvido por Oapo Design
							<img src="img/footer/logo.png" alt="Oapo Design" title="Oapo Design">
						</a>
					</li>
				</ul>
			</div>
		</aside>
	</footer>
	<script src="js/home.js"></script>
	<script src="js/o-ninho.js"></script>
	<script src="js/atividades.js"></script>
	<script src="js/galeria-interna.js"></script>
	<script src="js/hospedagem.js"></script>
	<script src="js/perguntas.js"></script>
	<script src="js/turismo.js"></script>
	<script src="js/tab-turismo.js"></script>
	<script src="js/mural.js"></script>
	<script src="js/noticias.js"></script>
	<script src="js/mapa-atividades.js"></script>
	<script src="js/mapa-contato.js"></script>
	<script src="js/imprimir-mapa.js"></script>
</body>
</html>