<?php include 'header.php';?>
	<section id="content" class="noticias">
		<!-- MATAGAL -->
		<article class="fotos-noticias">
			<figure>
				<img class="lazy" src="img/noticias/fotos.png" alt="">
			</figure>
		</article>
		<article class="capaceteRight">
			<figure>
				<img class="lazy" src="img/noticias/capacete.png" alt="">
			</figure>
		</article>
		<article class="bike-noticias">
			<figure>
				<img class="lazy" src="img/noticias/bike.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/mural/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-contato">
				<h1>Notícias</h1>
			</div>
			
			<div class="registro">
				<div class="noticias">
		
					<h3>Socorro realiza Campeonato Brasileiro de Rafting entre os dias 22 e 24 de março</h3>

					<img src="img/noticias/foto-interna.jpg" alt="" class="img-interna">

					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse vel suscipit tortor. Aenean dictum sapien ac tempor ultrices. Phasellus vel ipsum arcu. Maecenas tincidunt magna metus, at dapibus odio convallis nec. Nullam varius nunc ipsum, eu vehicula nisl ornare non. Aenean adipiscing rhoncus dolor, non bibendum metus sollicitudin sed. Praesent hendrerit a massa quis consectetur.</p>
					<p>Donec convallis lorem eget consequat commodo. Nunc tincidunt nulla massa, tincidunt fringilla libero auctor non. Fusce sed tellus nisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam massa sapien, ultrices scelerisque mauris sed, tempus sodales urna. Vestibulum dapibus lorem leo, sed fermentum eros commodo nec. Maecenas sagittis augue sit amet quam sollicitudin, et molestie justo porttitor. Integer rutrum massa orci, vitae interdum tellus adipiscing tristique. Praesent blandit, justo id semper varius, est sem laoreet nisi, eu fermentum nunc urna ut ante. Nam mauris nunc, sollicitudin pretium malesuada in, feugiat ac dui. Nam quis mattis libero. Donec sed dui ut erat porttitor interdum nec id nibh. Vivamus tristique turpis et nisi volutpat, ut lobortis velit faucibus. Pellentesque ut sem vulputate odio mattis porttitor. Curabitur pretium felis non ipsum dignissim lobortis. Morbi tempor ullamcorper diam at aliquam.</p>
					<p>Sed lacinia lacus hendrerit ante congue, sed tempor magna fermentum. Aliquam ac est nisi. Curabitur non dictum ligula, id tincidunt libero. Vivamus et lectus sollicitudin, auctor massa id, consectetur lacus. Cras rhoncus dapibus ornare. Ut auctor, magna nec tempor sagittis, lorem odio fermentum sapien, at pellentesque mauris tortor quis diam. Donec sed ligula nec diam adipiscing fermentum quis quis mi. Donec rutrum erat non dictum aliquam. Vestibulum ut libero vitae mi semper scelerisque.</p>
					<p>Aenean nisi orci, tincidunt a arcu a, facilisis malesuada sem. Aenean sodales nibh velit, elementum pretium felis convallis quis. Mauris accumsan ornare facilisis. Morbi mattis odio in adipiscing rutrum. Nam gravida viverra pulvinar. Praesent malesuada purus augue. In vulputate, nulla a congue euismod, enim nisi tincidunt nunc, in iaculis neque sem non neque. Sed gravida accumsan interdum. Fusce ac dolor turpis. Fusce ullamcorper velit a lorem eleifend fringilla ac nec magna. Maecenas pharetra vehicula turpis. Phasellus eu rhoncus mi. Mauris sit amet sem hendrerit sapien aliquam rhoncus suscipit id nibh. Quisque eget posuere augue.</p>
					<p>Cras felis ante, elementum nec lacinia in, molestie eu mauris. Sed imperdiet mauris nec lobortis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In vitae erat ac diam euismod placerat. Vestibulum et leo quis augue varius aliquam ut vel magna. Proin rhoncus tellus sem, quis eleifend velit faucibus in. Sed hendrerit sagittis magna vel vulputate. Nunc adipiscing, ligula a elementum pharetra, sapien purus tempor diam, eget sollicitudin erat neque quis ligula. Praesent elementum euismod commodo. Donec dapibus leo sapien, a adipiscing tellus congue id. Quisque venenatis interdum suscipit. Suspendisse potenti. Praesent pharetra a metus sit amet vestibulum.</p>
					<ul>
						<li>Compartilhe!:</li>
						<li>+</li>
						<li>Twiiter</li>
						<li>pinterest</li>
						<li>Google Plus</li>
						<li>Facebook</li>
					</ul>
				</div>




				<div class="sidebar">
					<div class="tags">
						<h1>Tags</h1>
						palavras de tags, os tamanhos varia de acordo com palavras cadastradas e também no back-end
					</div>
					<div class="arquivos">
						<h1>Arquivo</h1>
						<select name="arquivo" id="arquivo" class="styled">
							<option value="0">Selecione um periodo</option>
							<option value="1">2013</option>
							<option value="1">2012</option>
							<option value="1">2011</option>
							<option value="1">2010</option>
						</select>
						<input type="submit" name="filtra-arquivo">
					</div>
					<div class="mais-lidos">
						<h1>Mais Lidos</h1>
						<div>
							<a href="#">
								<img src="img/noticias/mais.jpg" alt="">
							</a>
							<strong>Canoagem alia aventura e apreciação da natureza</strong>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor, ipsum eget aliquam massa nunc.</p>
						</div>

						<div>
							<a href="#">
								<img src="img/noticias/mais.jpg" alt="">
							</a>
							<strong>Canoagem alia aventura e apreciação da natureza</strong>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor, ipsum eget aliquam massa nunc.</p>
						</div>

						<div>
							<a href="#">
								<img src="img/noticias/mais.jpg" alt="">
							</a>
							<strong>Canoagem alia aventura e apreciação da natureza</strong>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor, ipsum eget aliquam massa nunc.</p>
						</div>

						<div>
							<a href="#">
								<img src="img/noticias/mais.jpg" alt="">
							</a>
							<strong>Canoagem alia aventura e apreciação da natureza</strong>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc auctor, ipsum eget aliquam massa nunc.</p>
						</div>
					</div>
				</div>
			</div>
			
			
<?php include 'footer.php';?>