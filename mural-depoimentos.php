<?php include 'header.php';?>
	<section id="content" class="mural">
		<!-- MATAGAL -->
		<article class="fotos-mural">
			<figure>
				<img class="lazy" src="img/mural/fotos.png" alt="">
			</figure>
		</article>
		<article class="mochileiro">
			<figure>
				<img class="lazy" src="img/mural/mochileiro.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/mural/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-mural">
				<h1>
					Depoimentos
				</h1>
				<a href="envia-mural.php" class="envie">
					Enviar
				</a>
				<div class="depoimento-destaque">
					<div class="aspas"></div>
					<div class="aspas"></div>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae,<br>convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu<br>cursus consectetur.
					<div class="ass">
						<span>Nome Sobrenome</span> - Local UF
					</div>
				</div>
			</div>

			<div class="mural-depoimento">
				
				<div class="registros">
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					
					<div class="page">
						<ul>
							<li>
								<a href="#" class="selected">1</a>
							</li>
							<li>
								<a href="#">2</a>
							</li>
							<li>
								<a href="#">3</a>
							</li>
							<li>
								<a href="#">4</a>
							</li>
						</ul>
					</div>

				</div>
				
			</div>

			
				

			
				


			
		

			
<?php include 'footer.php';?>