<?php include 'header.php';?>
	<section id="content" class="mural">
		<!-- MATAGAL -->
		<article class="fotos-mural">
			<figure>
				<img class="lazy" src="img/mural/fotos.png" alt="">
			</figure>
		</article>
		<article class="mochileiro">
			<figure>
				<img class="lazy" src="img/mural/mochileiro.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->

		<article class="banner-interno">
			<img src="img/mural/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-mural">
				<h1>Mural</h1>
				<div class="frase">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae,<br>convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu<br>cursus consectetur.
				</div>
			</div>

			<div class="mural-depoimento">
				<div class="titulo">
					<h1>Depoimentos</h1>
				</div>
				<div class="more">
					<ul>
						<li>
							<a href="mural-depoimentos.php" class="veja">
								Veja a Galeria Completa
							</a>
						</li>
						<li>
							<a href="envia-mural.php" class="envie">
								Enviar
							</a>
						</li>
					</ul>
				</div>
				<div class="registros">
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
					<div class="entry">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus porttitor, molestie augue vitae, convallis neque. Proin a nibh ut lorem aliquam vulputate ut in massa. Vestibulum bibendum magna eu cursus consectetur.</p>
						<span>Nome Sobrenome</span> - Local UF
					</div>
				</div>
			</div>

			<div class="mural-depoimento-fotos">
				<div class="titulo">
					<h1>Fotos</h1>
				</div>
				<div class="more">
					<ul>
						<li>
							<a href="mural-fotos.php" class="veja">
								Veja a Galeria Completa
							</a>
						</li>
						<li>
							<a href="envia-mural.php" class="envie">
								Enviar
							</a>
						</li>
					</ul>
				</div>
				<div class="registros-fotos">
					
					<ul id="mycarousel" class="jcarousel-skin-tango">
							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/mural/hover.png" alt="">
										</a>
									</div>
									<img src="img/mural/foto1.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/mural/hover.png" alt="">
										</a>
									</div>
									<img src="img/mural/foto1.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/mural/hover.png" alt="">
										</a>
									</div>
									<img src="img/mural/foto1.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/mural/hover.png" alt="">
										</a>
									</div>
									<img src="img/mural/foto1.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/mural/hover.png" alt="">
										</a>
									</div>
									<img src="img/mural/foto1.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
											<img src="img/mural/hover.png" alt="">
										</a>
									</div>
									<img src="img/mural/foto1.jpg" alt="">
								</div>
							</li>
													
						</ul>

				</div>
			</div>

			<div class="mural-depoimento-videos">
				<div class="titulo">
					<h1>Videos</h1>
				</div>
				<div class="more">
					<ul>
						<li>
							<a href="mural-videos.php" class="veja">
								Veja a Galeria Completa
							</a>
						</li>
						<li>
							<a href="envia-mural.php" class="envie">
								Enviar
							</a>
						</li>
					</ul>
				</div>
				<div class="registros-videos">
					

					<ul id="mycarousel2" class="jcarousel-skin-tango">

							<li>
								<div class="item">
									<div class="hover">
										<a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="fancybox2" data-fancybox-group="gallery2">
											<img src="img/mural/hover-video.png" alt="">
										</a>
									</div>
									<img src="img/mural/video.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="fancybox2" data-fancybox-group="gallery2">
											<img src="img/mural/hover-video.png" alt="">
										</a>
									</div>
									<img src="img/mural/video.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="fancybox2" data-fancybox-group="gallery2">
											<img src="img/mural/hover-video.png" alt="">
										</a>
									</div>
									<img src="img/mural/video.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="fancybox2" data-fancybox-group="gallery2">
											<img src="img/mural/hover-video.png" alt="">
										</a>
									</div>
									<img src="img/mural/video.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="fancybox2" data-fancybox-group="gallery2">
											<img src="img/mural/hover-video.png" alt="">
										</a>
									</div>
									<img src="img/mural/video.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="fancybox2" data-fancybox-group="gallery2">
											<img src="img/mural/hover-video.png" alt="">
										</a>
									</div>
									<img src="img/mural/video.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="fancybox2" data-fancybox-group="gallery2">
											<img src="img/mural/hover-video.png" alt="">
										</a>
									</div>
									<img src="img/mural/video.jpg" alt="">
								</div>
							</li>
							<li>
								<div class="item">
									<div class="hover">
										<a href="http://www.youtube.com/watch?v=opj24KnzrWo" class="fancybox2" data-fancybox-group="gallery2">
											<img src="img/mural/hover-video.png" alt="">
										</a>
									</div>
									<img src="img/mural/video.jpg" alt="">
								</div>
							</li>


						</ul>


				</div>
			</div>


			
		

			
<?php include 'footer.php';?>