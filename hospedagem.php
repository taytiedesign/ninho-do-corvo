<?php include 'header.php';?>
	<section id="content" class="hospedagem">
		<!-- MATAGAL -->
		<article class="rede">
			<figure>
				<img src="img/hospedagem/rede.png" alt="">
			</figure>
		</article>
		
		<article class="camping">
			<figure>
				<img class="lazy" src="img/hospedagem/camping.png" alt="">
			</figure>
		</article>
		<article class="mato1">
			<figure>
				<img src="img/body/mato1.png" alt="">
			</figure>
		</article>
		<article class="mato2">
			<figure>
				<img src="img/body/mato2.png" alt="">
			</figure>
		</article>
		<article class="mato3">
			<figure>
				<img class="lazy" src="img/body/mato3.png" alt="">
			</figure>
		</article>
		<article class="mato4">
			<figure>
				<img class="lazy" src="img/body/mato4.png" alt="">
			</figure>
		</article>
		<!-- MATAGAL -->
		<article class="banner-interno">
			<img src="img/hospedagem/topo.png" alt="">
		</article>

		<article class="middle clearfix">
			
			<div class="intro-hospedagem">
				<h1>Ninho do Corvo Bed, Breakfast & Dinner</h1>
				<div class="texto">
					<p>A RPPN tem capacidade de acomodar pequenos grupos de até 4 pessoas, que desejam passar por  momentos agradáveis junto a natureza com  conforto e exclusividade. Atendida por seus proprietários o Ninho do Corvo BB&D, oferece a seus hóspedes toda a tranqüilidade do local que aliado ao atendimento exclusivo os fazem sentirem-se em casa. </p>
					<p>A noite, sempre tem  uma deliciosa comida caseira com  grelhados preparados na brasa.  Pela manhã, um café da manhã reforçado para quem vai gastar muita energia nas trilhas ou fazendo atividades com a </p>
				</div>
				<div class="texto">
					<p>banho de rio, ou mesmo ficar relaxando junto a natureza. Enfim opções para os nossos hóspedes é o que não falta.</p>
					<p>Temos um  apartamento  composto de uma suíte para casal, com banheiro e uma bela vista do Vale do Corvo da janela, e mais um quarto duplo/casal. Podemos dispor do apartamento para um grupo, ou também podemos dispor dos quartos de forma individual, tendo assim capacidade de acomodar grupos desde 1 até 4 pessoas.</p>
				</div>
			</div>

			<div class="galeria-hospedagem">
				<div class="content-hospedagem">
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
				</div>
			</div>

			<div class="banner-middle-hospedagem">
				<img src="img/hospedagem/middle.png" alt="">
			</div>

			<div class="fotos3">
				<h1>Acampamento Base Ninho do Corvo</h1>
				<p>A importância da reserva se deve além da beleza cênica do local, com varias cachoeiras, ela guardar espécies da flora e fauna em perigo de extinção como  xaxins, imbuias, araucárias, bem como é uma área de grande atrativo geológico devido as suas formações de basalto ao longo do curso do rio Barra Bonita. É importante também ressaltar que os proprietários entendem que a grande vocação da região é para o ecoturismo e  turismo sem conservacionismo é algo praticamente impossível e a RPPN tem também como missão passar os conceitos de conservação e utilização sustentável para a comunidade, de forma que esta seja,  através do exemplo,  objeto de transformação da mesma no que se refere a estes temas.</p>
			</div>

			<div class="galeria-hospedagem2">
				<div class="content-hospedagem">
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
					<div class="entry">
						<div class="hover">
							<a href="img/1_b.jpg" class="fancybox" data-fancybox-group="gallery">
								<img src="img/hospedagem/hover.png" alt="">
							</a>
						</div>
						<img src="img/hospedagem/foto.jpg" alt="">
					</div>
				</div>
			</div>
			

<?php include 'footer.php';?>